class Main {
  constructor() {
    this.$butOk = document.querySelector('.ok');
    this.$fieldUl = document.querySelector('ul');
    this.$butChancel = document.querySelector('.cancel');
    this.$fieldAdd = document.querySelector('.wrapp-but');
    this.$inputfield = document.querySelector('.addTask');
    this.$operations = document.querySelector('.wrapp-operation');
    this.$butDone = document.querySelector('.done');
    this.$butDelete = document.querySelector('.delete');
    this.$butInProcces = document.querySelector('.in-procces');

    this.todo = new Todo();
    this.cart = new Cart();

    this.id = 0;

    this.$fieldUl.addEventListener('click',this.addfieldOperations.bind(this));
    this.$butOk.addEventListener('click',this.addTodo.bind(this));
    this.$butChancel.addEventListener('click',this.clearInput.bind(this));

    this.$butDelete.addEventListener('click',this.deleteTask.bind(this));
    this.$butDone.addEventListener('click',this.doneTask.bind(this));
    this.$butInProcces.addEventListener('click',this.inProcces.bind(this));

  }
  loadStorage() {
    if(window.localStorage.length !== 0) {
      const listInCart = this.cart.getList();
      let count = 0
      for (let task of listInCart) {
        let todo = new Todo(task.text,task.id);
        this.$fieldUl.appendChild(todo.$elem);
      }
    }
  }

  addTodo() {
    if(this.$inputfield.value.trim() === '') {
      this.clearInput();
    } else {
      this.getId();
      const listInCart = this.cart.getList();
      let todo = new Todo(this.$inputfield.value,this.id);
      this.$fieldUl.appendChild(todo.$elem);
      this.clearInput();
      this.cart.addToCart(todo);
    }
  }

  getId() {
    if(window.localStorage.length === 0) {
      return this.id;
    } else {
        const listInCart = this.cart.getList();
        for(let task of listInCart) {
          if (this.id === task.id) {
            this.id++;
            this.getId();
          }
        }
      return this.id;
    }
  }
  clearInput() {
    this.$inputfield.value = '';
  }
  deleteTask(event) {
    const elemActiveLi = event.target.parentNode.previousElementSibling;
    const modelLi = this.findElem(elemActiveLi,this.cart.getList());
    const result = confirm('Вы дествительно хотите удалить эту задачу?');
    if(result) {
      this.$fieldUl.removeChild(elemActiveLi);
      this.$fieldUl.removeChild(this.$operations);
      this.cart.removeFromCart(modelLi);
    }
  }
  inProcces() {
    const elemActiveLi = event.target.parentNode.previousElementSibling;
    this.todo.doTask(elemActiveLi);
  }
  doneTask() {
    const elemActiveLi = event.target.parentNode.previousElementSibling;
    this.todo.finishTask(elemActiveLi);
  }
  addfieldOperations(event) {
    const activeLi = event.target;
    if (this.lastActiveElem === activeLi) {
      this.$fieldUl.removeChild(this.$operations);
      this.lastActiveElem = '';
    } else if (activeLi.tagName === 'LI') {
      this.$fieldUl.insertBefore(this.$operations, activeLi.nextSibling);
      this.lastActiveElem = activeLi;
    } else { return }
    this.$operations.classList.remove('hide');
  }
  findElem(elem,arr) {
    return arr.find(check => check.id === +elem.dataset.id);
  }
}
class Todo {
  constructor(text,id) {
    this.$elem = document.createElement('li');
    this.$elem.setAttribute('data-id',id);
    this.$elem.innerText = text;
    this.text = text;
    this.id = id;
  }
  finishTask(elem) {
    elem.classList.add('doneLi');
  }
  doTask(elem) {
    elem.classList.remove('doneLi');
  }
}

class Cart {
  constructor() {
    this.storage = {
      list: [],
      get tasks() {
        if(window.localStorage.tasks) {
          this.list = JSON.parse(localStorage.getItem('tasks'));
        } else {
          localStorage.setItem('tasks', JSON.stringify([]));
          this.list = [];
        }
        return this.list;
      },
      set tasks(value) {
        localStorage.setItem('tasks', JSON.stringify(value));
        console.log('set');
      }
    };
  }
  addToCart(task) {
    this.storage.tasks = [...this.storage.tasks, task]; 
  }
  getList() {
    return this.storage.tasks;
  }
  removeFromCart (productInCard) {
    let indexEl = this.storage.tasks.findIndex((list) => list.id === productInCard.id);
    const array = [...this.storage.tasks];
    array.splice(indexEl, 1);
    this.storage.tasks = array;
  }
}

document.addEventListener('DOMContentLoaded',()=> {
    this.main = new Main();
    this.main.loadStorage();
});      